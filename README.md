#  open SDK 示範

_version update 2023-10-30_

### 修改日志

| 修改日期   | 修改内容                                                     |
| ---------- | ------------------------------------------------------------ |
| 2023-10-30 | 1.輕量SDK Update |

 
## 說明

主分支提供SDK所有版本分支快速連結，各分支摆放相关文件与示范。

## 资源连结

[Android Java](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/android-java)

此分支提供 Java 版本對接示范，使用兩個 Fragment 方式，FirstFragment模擬用户主项目，SecondFragment示范BGSDK衔接方式。

[Android Kotlin](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/android-kotlin)

此分支提供 Kotlin 版本對接示范，使用兩個 Fragment 方式，FirstFragment模擬用户主项目，SecondFragment示范BGSDK衔接方式。

[IOS OC++](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/ios-oc)

此分支提供 OC++ 版本對接示范，使用兩個 ViewController 方式调用，ViewController模擬用户主项目，ViewController2示范BGSDK衔接方式。

[IOS Swift](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/ios-swift)

此分支提供 Swift 版本對接示范，使用兩個 ViewController 方式调用，ViewController为用户主项目，ViewController2示范BGSDK衔接方式。

[H5-bundle](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/h5-bundle)

此分支为h5-bundle 资源包，客制产出资源适用于 ios or android 版本h5-sdk 资源包。

# -- ENGLISH VERSION --

### Change Log

| Updated DateTime | Updated Content  |
| ---------------- | ---------------- |
| 2023-10-30       | 1.opensdk-mini update |

## Description

The main branch provides quick links to all versions of the SDK, with each branch containing relevant files and demonstrations.

## Resources link

[Android Java](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/android-java#-english-version-)

This branch contains a Java Example. The FirstFragment simulates the user's main project, while the SecondFragment demonstrates the integration of the BGSDK.

[Android Kotlin](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/android-kotlin#-english-version-)

This branch contains a Kotlin Example. The FirstFragment simulates the user's main project, while the SecondFragment demonstrates the integration of the BGSDK.

[IOS OC++](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/ios-oc#-english-version-)

This branch contains a Object-C Example. The ViewController simulates the user's main project, while ViewController2 demonstrates the integration of the BGSDK.

[IOS Swift](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/ios-swift#-english-version-)

This branch contains a Swift Example. The ViewController represents the user's main project, while ViewController2 demonstrates the integration of the BGSDK.

[H5-bundle](https://gitlab.com/BigGameVideoTeam/opensdk-mini/-/tree/h5-bundle#-english-version-)

This branch contains h5-bundle resource packages customized for iOS or Android versions of the h5-sdk resource package.
